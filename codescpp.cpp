﻿#include <iostream>
#include <string>
#include <vector>
using namespace std;

void gray(int n)
{
    if (n <= 0)
        return;

    // Хранит в себе сгенерированные коды
    vector<string> arr;

    // Начало для кодов
    arr.push_back("0");
    arr.push_back("1");

    // С каждым шагом количество кодов увеличивается в 2*i
    int i, j;
    for (i = 2; i < pow(2, n); i = i * 2)
    {
        // Переворачивает предыдущие сгенерированные коды и добавляет в конец 
        for (j = i - 1; j >= 0; j--)
            arr.push_back(arr[j]);

        // Добавление 0 спереди в первой части
        for (j = 0; j < i; j++)
            arr[j] = "0" + arr[j];

        // Добавление 1 спереди во второй части
        for (j = i; j < 2 * i; j++)
            arr[j] = "1" + arr[j];
    }

    for (i = 0; i < arr.size(); i++)
        cout << arr[i] << endl;
}

int main()
{
    int n;

    cout << "Input n: ";
    cin >> n;

    gray(n);

    return 0;
}